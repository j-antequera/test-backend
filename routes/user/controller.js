const models = require("../../models/nosql/user/user.ops");
const bcrypt = require('bcrypt');

module.exports = {
  read: async (req, res) => {console.log(req.userLogged);
    try {
      const { skip, limit, ...conditions } = { ...req.query };
      const limits = {
        skip: Number(skip) || 0,
        limit: Number(limit) || 10
      };
      const result = await models.read(limits, conditions);
      return res.json(result);

    } catch (error) {
      return res.json(error);
    }
  },
  insert: async (req, res) => {
    try {
      const user = { ...req.body };
      user.password = bcrypt.hashSync(user.password, 10);
      const result = await models.insert(user);
      return res.json(result);

    } catch (error) {
      return res.json(error);
    }
  },
  update: async (req, res) => {
    try {
      const { id } = { ...req.params };
      const user = { ...req.body };
      const result = await models.update(user, id);
      return res.json(result);
  
    } catch (error) {
      return res.json(error);
    }
  },
  remove: async (req, res) => {
    try {
      const { id } = { ...req.params };
      const result = await models.remove(id);
      return res.json(result);
  
    } catch (error) {
      return res.json(error);
    }
  },
};
