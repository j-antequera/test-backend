const app = require('express').Router();
const controller = require('./controller');
const { tokenAuthentication } = require('../../middlewares/authentication');

app.get('/:id?', tokenAuthentication, controller.read);
app.post('/', tokenAuthentication, controller.insert);
app.put('/:id', tokenAuthentication, controller.update);
app.delete('/:id', tokenAuthentication, controller.remove);

module.exports = app;