const app = require('express').Router();
const user = require('./user');
const login = require('./login');
const money = require('./money');

app.use('/user', user);
app.use('/login', login);
app.use('/money', money);

module.exports = app;