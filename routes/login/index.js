const app = require('express').Router();
const controller = require('./controller');

app.post('/', controller.login);

module.exports = app;