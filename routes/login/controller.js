const models = require("../../models/nosql/user/user.ops");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  login: async (req, res) => {
    try {
      const user = { ...req.body };
      const userRow = await models.login(user);

      if (userRow && bcrypt.compareSync(user.password, userRow.password)) {
        const result = {
          email: userRow.email,
          expiration: '3 days',
          token: jwt.sign({ user: userRow }, 'money-corp', { expiresIn: '3 days' })
        }

        return res.json(result);
      }

      return res.status(400).json({ Message: 'Email or password invalid'});

    } catch (error) {
      return res.json(error);
    }
  }

};
