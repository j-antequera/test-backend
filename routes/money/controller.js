const models = require("../../models/sql/money");

module.exports = {
  read: async (req, res) => {
    try {
      const result = await models.read();
      return res.json(result);

    } catch (error) {
      return res.json(error);
    }
  },
  insert: async (req, res) => {
    try {
      const { denomination } = { ...req.body };
      const result = await models.insert({ denomination });
      return res.json(result);

    } catch (error) {
      return res.json(error);
    }
  },
  update: async (req, res) => {
    try {
      const { id } = { ...req.params };
      const { denomination } = { ...req.body };
      const result = await models.update({ denomination }, id);
      return res.json(result);
  
    } catch (error) {
      return res.json(error);
    }
  },
  remove: async (req, res) => {
    try {
      const { id } = { ...req.params };
      const result = await models.remove(id);
      return res.json(result);
  
    } catch (error) {
      return res.json(error);
    }
  },
};
