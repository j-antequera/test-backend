const app = require('express').Router();
const controller = require('./controller');

app.get('/:id?', controller.read);
app.post('/', controller.insert);
app.put('/:id', controller.update);
app.delete('/:id', controller.remove);

module.exports = app;