const mongoose = require('../../../db/mongodb');
const Schema = mongoose.Schema;

const roles = {
  values: ['USER', 'ADMIN']
};

const user = new Schema({
    name: {
      type     : String,
      required : true
    },
    email: {
      type     : String,
      unique   : true,
      required : true
    },
    password: {
      type     : String,
      required : true
    },
    role: {
      type     : String,
      default  : 'USER',
      enum     : roles
    }
});

module.exports = mongoose.model('user', user);