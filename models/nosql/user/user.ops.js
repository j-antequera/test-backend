const User = require('./user.model');

module.exports = {
  read: async (limits, conditions) => {
    const { skip, limit } = limits;
    const result = await User.find(conditions).skip(skip).limit(limit);
    const count = await User.countDocuments(conditions);
    return { result, count };
  },

  insert: async (userData) => {
    const result = await User.create(userData);
    return result; 
  },

  update: async (userData, id) => {
    const result = await User.findByIdAndUpdate(id, userData, { new: true });
    return result; 
  },

  remove: async (id) => {
    const result = await User.findByIdAndRemove(id);
    return result; 
  },
  
  login: async (user) => {
    const result = await User.findOne({ email: user.email });
    return result; 
  },

};