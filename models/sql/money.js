const connection = require('../../db/myslq');

module.exports = {

  read: () => {
    return new Promise((resolve, reject) => {
      connection.query(`SELECT * FROM moneybox.money`, (error, results) => {

        if (error) reject(error);

        if (results.length) {
          resolve(results);
        }
        resolve('not results');
      });
    });
  },

  insert: (denomination) => {
    return new Promise((resolve, reject) => {
      connection.query(`INSERT INTO moneybox.money SET ?`, denomination, (error, results) => {

        if (error) reject(error);

        resolve('Money insert!');
      });
    });
  },

  update: (denomination, id) => {
    return new Promise((resolve, reject) => {
      connection.query(`UPDATE moneybox.money SET ? WHERE id=${id}`, denomination, (error, results) => {

        if (error) reject(error);

        resolve('Money update!');
      });
    });
  },

  remove: (id) => {
    return new Promise((resolve, reject) => {
      connection.query(`DELETE FROM moneybox.money WHERE id='${id}'`, (error, results) => {

        if (error) reject(error);

        resolve('Money remove!');
      });
    });
  },

}

