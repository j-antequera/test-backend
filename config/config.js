const environmentVariables = {
  production: {
    webServicePort : process.env.PORT,
  },
  development: {
    webServicePort : 3000,
  }
}

if (process.env.NODE_ENV === 'production') {
  module.exports = environmentVariables.production;
} else {
  module.exports = environmentVariables.development;
}