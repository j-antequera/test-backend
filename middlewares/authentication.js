const jwt = require('jsonwebtoken');

module.exports = {
  tokenAuthentication: (req, res, next) => {
    const token = req.get('token');

    jwt.verify(token, 'money-corp', (error, decoded) => {
      if (error) {
        return res.status(401).json(error);

      } else {
        req.userLogged = decoded.user;
        next();
      }
    })
  },

  roleAuthentication: (req, res, next) => {

  }
}