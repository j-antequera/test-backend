const app = require('./app');
const config = require('./config/config');
const webPort = config.webServicePort;

app.listen(webPort, () => console.log(`Server running in port ${webPort}`));