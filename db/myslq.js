const mysql = require('mysql');

const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'meinsm'
});

connection.connect((error) => {
	if (error) {
		console.log(error);
		throw error;
	}
});

connection.query("CREATE DATABASE IF NOT EXISTS moneybox", (err, result) => {
	if (err) throw err;
	console.log("Database created");

	connection.query("CREATE TABLE IF NOT EXISTS moneybox.money (id INT AUTO_INCREMENT PRIMARY KEY, denomination VARCHAR(255))", (err, result) => {
		if (err) throw err;
		console.log("Table created");
	});
});

module.exports = connection;