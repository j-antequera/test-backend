const mongoose = require('mongoose');
const connectUrl = 'mongodb://localhost:27017/moneybox';

const deprecationWarning = {
  useNewUrlParser:true, 
  useUnifiedTopology: true, 
  useCreateIndex: true
}

mongoose.connect(connectUrl, deprecationWarning, (error, res) => {
  if (error) {
    throw error;
  }
  console.log('connected to mongodb');
})

module.exports = mongoose;