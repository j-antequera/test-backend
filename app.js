const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./routes/routes');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  return next();
});

app.use('/api', api);

module.exports = app;